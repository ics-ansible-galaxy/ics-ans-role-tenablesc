# ics-ans-role-tenablesc

Ansible role to install tenablesc.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-tenablesc
```

## License

BSD 2-clause
