import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_port_and_service(host):
    test_port = host.socket("tcp://0.0.0.0:443").is_listening
    test_service = host.service("SecurityCenter")
    assert test_service
    assert test_port
